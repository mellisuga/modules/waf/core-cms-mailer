import Imap from 'imap';
import { inspect } from 'util';

import { MailParser } from 'mailparser-mit';
import MailComposer from 'nodemailer/lib/mail-composer/index.js';

export default class IMAP {
  static async construct(cfg, mailer) {
    try {
      console.log("IMAP:");
      let _this = new IMAP(cfg);

      let imap = _this.imap;

      await new Promise(function(resolve) {
        imap.once('ready', function() {
          resolve();
        });


        imap.once('error', function(err) {
          console.log(err);
        });

        imap.once('end', function() {
          console.log('Connection ended');
        });

        imap.connect();
      });

      _this.mailer = mailer;


      _this.box_clients = [];
      await _this.open_box("INBOX");

      let boxes = await _this.get_boxes();

      for (let b = 0; b < boxes.length; b++) {
        if (boxes[b].toLowerCase() != "inbox") {
          await _this.open_box(boxes[b]);
        }
      }


      console.log("IMAP DONE!");
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg) {
    this.cfg = cfg;

    this.imap = new Imap({
      user: this.cfg.user,
      password: this.cfg.pass,
      host: this.cfg.host,
      port: this.cfg.port,
      tls: true,
      connTimeout: 30000,
      authTimeout: 30000
    });
  }

  async open_box(boxname) {
    try {
      console.log("+", this.cfg.user, " OPENING MAILBOX ", boxname);
      let unread_count = 0;

      let imap = this.imap;



      let _this = this;

      this.box_clients.push(await new Promise(function(resolve) {
        imap.status(boxname, function(errs, box) {
          if (errs) throw errs;
          box.name = boxname.toLowerCase();
          box.unseen = box.messages.unseen;
          console.debug("+", "+", "TOTAL:", box.messages.total, " UNSEEN:", box.unseen);
          resolve(box);
        });
      }));

      this.no_unread = unread_count;
    } catch (e) {
      console.error(e.stack);
    }
  }
/*
  async next_box(name) {
    try {
      let imap = this.client;
      this.box = await new Promise(function(resolve) {
        imap.openBox(name, false, function(err, box) {
          if (err) throw err;
          resolve(box);
        });
      });
      return "OK";
    } catch (e) {
      console.error(e.stack);
    }
  }
*/
  async get_unread_count(boxname) {
    try {
      let unread_count = 0;
      let imap = this.imap;
      if (boxname) {
        let box = await this.get_box(boxname);
        unread_count += box.unseen;
      } else {
        for (let c = 0; c < this.box_clients.length; c++) {
          let box = this.box_clients[c];
          unread_count += box.unseen;
        }
      }

      return unread_count;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async get_box(boxname) {
    try {
      let imap = this.imap;
      for (let c = 0; c < this.box_clients.length; c++) {
        if (this.box_clients[c].name === boxname.toLowerCase()) {

          await new Promise(function(resolve) {
            imap.openBox(boxname, false, function(err, box) {
              if (err) throw err;
              resolve(box);
            });
          });

          return this.box_clients[c];
        }
      }
      return undefined;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async get_inbox(offset, limit, boxname) {
    try {

      let inbox = [];

      let imap = this.imap;
      let box = await this.get_box(boxname);


      let emin = box.messages.total-offset-limit+1;
      let emax = box.messages.total-offset;

      if (emin < 1) emin = 1;

      let unread_count = 0;

      await new Promise(function(resolve) {
        var f = imap.seq.fetch(emin+':'+emax, {
          bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)'],
          struct: true
        });
        f.on('message', function(msg, seqno) {
          let mesg_obj = {};
          msg.on('body', function(stream, info) {
            var buffer = '';
            stream.on('data', function(chunk) {
              buffer += chunk.toString('utf8');
            });
            stream.once('end', function() {
              mesg_obj.head = Imap.parseHeader(buffer);
            });
          });
          msg.once('attributes', function(attrs) {
            if (!attrs.flags.includes("\\Seen")) unread_count++;
            mesg_obj.uid = attrs.uid;
            mesg_obj.flags = attrs.flags;
          });
          msg.once('end', function() {
            inbox.push(mesg_obj);
          });
        });
        f.once('error', function(err) {
          console.log('Fetch error: ' + err);
        });
        f.once('end', function() {
          resolve();
        });
      });

      this.no_unread = unread_count;
      
      inbox.sort(function(a, b) {
        return b.uid - a.uid;
      });

      return inbox;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async get_boxes() {
    try {
      let imap = this.imap;

      let existing_boxes = await new Promise(function(resolve) {
        imap.getBoxes(function(erro, boxes) {
          if (erro) throw erro;
          let oboxes = [];
          for (let name in boxes) {
            oboxes.push(name);
          }
          resolve(oboxes);
        });
      });

      if (!existing_boxes.includes("Sent")) {
        await new Promise(function(resolve) {
          imap.addBox("Sent", function(err) {
            if (err) throw err;
            existing_boxes.push("Sent");
            resolve();
          });
        });
      }

      if (!existing_boxes.includes("Trash")) {
        await new Promise(function(resolve) {
          imap.addBox("Trash", function(err) {
            if (err) throw err;
            existing_boxes.push("Trash");
            resolve();
          });
        });
      }

      return existing_boxes;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async get_email(uid, boxname) {
    try {
      let imap = await this.get_box(boxname);
      let email = await new Promise(function(resolve) {
        imap.search([["UID", uid]], function(err, results) {
          if (err) throw err;
          var f = imap.fetch(results, { bodies: '' });
          f.on('message', function(msg, seqno) {
            msg.on('body', function(stream, info) {
              var buffer = '';
              stream.on('data', function(chunk) {
                buffer += chunk.toString('utf8');
              });
              stream.once('end', function() {
                imap.addFlags(uid, "\Seen", function(ferr) {
                  if (ferr) throw ferr;

                  let mailparser = new MailParser();
                  mailparser.on("end", function(mail_object) { 
                    resolve(mail_object.html);
                  });
                  mailparser.write(buffer.toString());
                  mailparser.end();
                });
              });
            });
          });
          f.once('error', function(err) {
            console.log('Fetch error: ' + err);
          });
        });
      });

      return email;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async move_email(uids, destbox, boxname) {
    try {
      let imap = this.imap;

      await new Promise(function(resolve) {
        imap.move(uids, destbox, function(ferr) {
          if (ferr) throw ferr;
          resolve();
        });
      });


      return await this.get_unread_count()+"";
    } catch (e) {
      console.error(e.stack);
    }
  }

  async set_email_flag(uids, flag, add, boxname) {
    try {
      let imap = this.imap;

      await new Promise(function(resolve) {
        if (add) {
          imap.addFlags(uids, flag, function(ferr) {
            if (ferr) throw ferr;
            resolve();
          });
        } else {
          imap.delFlags(uids, flag, function(ferr) {
            if (ferr) throw ferr;
            resolve();
          });
        }
      });


      if (flag == "\Deleted") {
        console.log("EXPUNGE");
        await new Promise(function(resolve) {
          imap.expunge(uids, function(err) {
            if (err) throw err;
            resolve();
          });
        });
      }

      return await this.get_unread_count()+"";
    } catch (e) {
      console.error(e.stack);
    }
  }

  async save_sent(options, boxname) {
    try {
      let imap = this.imap;

      let mail = new MailComposer(options).compile();

      await new Promise(function(resolve) {
        mail.build(function(err, message){
          if (err) throw err;

          imap.append(message, { mailbox: 'Sent', flags: ['\Seen'], date: mail.date}, function(erro) {
            if (erro) throw erro;
            resolve();
          });
        });

      })
    } catch (e) {
      console.error(e.stack);
    }
  }
}
