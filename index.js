
import socket_io from 'socket.io';

import IMAP from './imap/index.js';
import SMTP from './smtp/index.js';

import multer from 'multer';

export default class Mailer {
  static async construct(cms, cfg) {
    try {
      let _this = new Mailer(cfg, cms);


      let auth_cfg = cms.get_module_cfg("flyauth");

      if (auth_cfg && auth_cfg.mailbot) {
        auth_cfg.mailbot_user_id = cfg.prefix+"_flyauth_mailbot";
        await _this.session([{
          user: auth_cfg.mailbot.user,
          pass: auth_cfg.mailbot.pass,
          name: auth_cfg.mailbot.name,
        }], auth_cfg.mailbot_user_id);

        cms.mail_session = auth_cfg.mail_session = _this.get_session(auth_cfg.mailbot_user_id, auth_cfg.mailbot.user);

      }

      if (cfg.public_email) {
        _this.contact_addr = cfg.public_email.contact_addr || cfg.public_email.address;
        await _this.session([{
          user: cfg.public_email.address,
          pass: cfg.public_email.pass
        }], cfg.prefix+"_public");

      //  auth_cfg.mail_session = _this.get_session(auth_cfg.mailbot_user_id, auth_cfg.mailbot.user);
      }
      
      _this.init_controls(cms.app, cms.admin.auth);


      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, cms) {
    console.log("MAILER INITIALIZING!", cfg);
    this.cfg = cfg;
    this.sessions = [];
    this.addrs = [];
    this.connecting = [];
    this.cms = cms;
    this.sockets = [];
    this.public_email_address = cfg.public_email ? cfg.public_email.address : undefined;
  }

  async session(emails, user_id) {
    try {
      if (emails && Array.isArray(emails) && !this.connecting.includes(user_id) && !this.get_session(user_id)) {
        console.log("MAILER START IMAP & SMTP SESSIONS");

        this.connecting.push(user_id);


        let email_session = {
          user_id: user_id,
          accounts: []
        };

        for (let e = 0; e < emails.length; e++) {
          let email = emails[e];
          this.addrs.push(email.user);
          let account = {
            address: email.user
          }


          account.imap = await IMAP.construct({
            host: this.cfg.imap.host,
            port: this.cfg.imap.port,
            user: email.user,
            pass: email.pass
          }, this);

          account.smtp = await SMTP.construct({
            host: this.cfg.smtp.host,
            port: this.cfg.smtp.port,
            name: email.name,
            user: email.user,
            pass: email.pass
          }, this, account.imap);

          email_session.accounts.push(account);
        }

        this.sessions.push(email_session);

        this.connecting.splice(this.connecting.indexOf(user_id), 1);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  get_session(uid, addr) {
    console.log("GET SESSION", uid, addr);
//    sloop:
    for (let s = 0; s < this.sessions.length; s++) {
 //     console.log("EXISTING", this.sessions[s].user_id);

  //    if (this.sessions[s].user_id === uid) {
        let email_session = this.sessions[s];
        for (let a = 0; a < email_session.accounts.length; a++) {
          if (email_session.accounts[a].address === addr) {
            return email_session.accounts[a]
      //      break sloop;
            break;
          }
        }
    //  }
    }
    return undefined;
  }

  init_controls(app, auth) {
    let _this = this;
    app.post("/content-manager/mailer.io", auth.orize_gen(["content_management"]), async function(req, res) {
      try {
        let data = JSON.parse(req.body.data);
        let user_id = req.session_data.id;

        switch (data.command) {
          case "all_addrs":
            res.json(_this.addrs);
            break;
          case "get_boxes":
            res.send(await _this.get_session(user_id, data.addr).imap.get_boxes());
            break;
          case "open_box":
            res.send(await _this.get_session(user_id, data.addr).imap.next_box(data.name, data.boxname)); 
            break;
          case "inbox_max":
            res.send((await _this.get_session(user_id, data.addr).imap.get_box(data.boxname)).messages.total+"");
            break;
          case "inbox":
            res.json(await _this.get_inbox(data.offset, data.limit, user_id, data.addr, data.boxname));
            break;
          case "email":
            res.send(await _this.get_session(user_id, data.addr).imap.get_email(data.uid, data.boxname));
            break;
          case "delete":
            //res.send(await _this.get_session(user_id).imap.set_email_flag(data.uids, "\Deleted", true));
            res.send(await _this.get_session(user_id, data.addr).imap.move_email(data.uids, "Trash", data.boxname));
            break;
          case "unread":
            res.send(await _this.get_session(user_id, data.addr).imap.set_email_flag(data.uids, "\Seen", false, data.boxname));
            break;
          case "read":
            res.send(await _this.get_session(user_id, data.addr).imap.set_email_flag(data.uids, "\Seen", true, data.boxname));
            break;
          case "unstar":
            res.send(await _this.get_session(user_id, data.addr).imap.set_email_flag(data.uids, "\Flagged", false, data.boxname));
            break;
          case "star":
            res.send(await _this.get_session(user_id, data.addr).imap.set_email_flag(data.uids, "\Flagged", true, data.boxname));
            break;
          case "send":
            res.send(await _this.get_session(user_id, data.addr).imap.get_email(data.uid, data.boxname));
            break;
          default:
            res.status(400).send("ERROR: BAD REQUEST! INVALID COMMAND!");
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    app.post("/mailer.io-send-public", async function(req, res) {
      try {
        console.log("SENDE PUB");
        console.log(req.body.data);
        
        let header_params = JSON.parse(decodeURIComponent(req.body.data));
        let email_session = await _this.get_session(false, _this.public_email_address)
        let email_options = {
          from: header_params.from,
          to: _this.contact_addr,
          subject: header_params.subject,
          text: header_params.text,
          html: header_params.text,
        }
        res.json(await email_session.smtp.send_email(email_options));
      } catch (e) {
        console.error(e.stack);
      }
    });

    var storage = multer.memoryStorage()
    var upload = multer({ storage: storage })
    app.post("/content-managemer/mailer.io/send", auth.orize_gen(["content_management"]), upload.array('attachments'), async function(req, res) {
      try {
        let header_params = JSON.parse(decodeURIComponent(req.headers.params));
        let user_id = req.session_data.id;
        let email_session = await _this.get_session(user_id, header_params.eaddr);

        let attachments = [];
        for (let f = 0; f < req.files.length; f++) {
          attachments.push({
            filename: req.files[f].originalname,
            content: req.files[f].buffer
          });
        }

        let email_options = {
          from: email_session.address,
          to: header_params.email_to,
          subject: header_params.email_subject,
          text: header_params.email_content,
          html: header_params.email_content,
          attachments: attachments 
        }

        res.send(await email_session.smtp.send_email(email_options, header_params.boxname));
      } catch (e) {
        console.error(e.stack);
      }
    });

    let io = socket_io(this.cms.app.http_server, {
      path: "/mailer.io"
    });
    io.use(auth.orize_socket);

    io.use(function(socket, next) {
      console.log(">>>>>>>>>>>>>>>>> SOCKET !!!");
      _this.sockets.push(socket);
      next();
    });

    console.log("MAILER READY!");
  }

  async get_inbox(offset, limit, user_id, eaddr, boxname) {
    try {
      let esess = this.get_session(user_id, eaddr);
      return await esess.imap.get_inbox(offset, limit, boxname);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
