
import nodemailer from 'nodemailer';

export default class SMTP {
  static async construct(cfg, mailer, imap) {
    try {
      console.log("SMTP:");
      let _this = new SMTP(cfg);
      _this.mailer = mailer;
      _this.imap = imap;
      
      console.log("+", cfg.user, "CONNECTING TO", cfg.host+":"+cfg.port);
      _this.transporter = nodemailer.createTransport({
        host: cfg.host,
        port: cfg.port,
        secure: cfg.port == 465 ? true : false,
        auth: {
          user: cfg.user,
          pass: cfg.pass
        }
/*          tls: {
          rejectUnauthorized: false
        }*/
      });

      await new Promise(function(resolve) {
        function timeout() {
          console.error("SMTP connection timeout!");
          resolve();
        }

        let timeout_ms = cfg.timeout || 5000;

        const timeout_id = setTimeout(timeout, timeout_ms);
        _this.transporter.verify(function(error, success) {
          if (error) {
            console.log(error);
          } else {
            clearTimeout(timeout_id);
            resolve();
          }
        });
      });

      console.log("SMTP DONE!");
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg) {
    this.cfg = cfg;
  }

  async send_email(options, save) {
    try {
      let _this = this;

      if (!options.from) options.from = this.cfg.user;
      if (this.cfg.name) options.from = this.cfg.name+"<"+this.cfg.user+">";

      console.log("MAIL OPTS", options);
      
      let resolved =  await new Promise(function(resolve) {
        _this.transporter.sendMail(options, (error, info) => {
          if (error) {
            resolve(error);
            return console.error(error);
          }
          console.log('Message sent: %s', info.messageId);
          // Preview only available when sending through an Ethereal account
          console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

          resolve({ success: true });
        });
      });

      if (save) await _this.imap.save_sent(options);
      return resolved;
    } catch (e) {
      console.error(e.stack);
    }
  }
}
